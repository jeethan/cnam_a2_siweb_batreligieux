<?php

use Router\Router;
use App\Exceptions\NotFoundException;

require '../vendor/autoload.php';

define('TWIG_CACHE', dirname(__DIR__). DIRECTORY_SEPARATOR . "cache" . DIRECTORY_SEPARATOR . "twig" . DIRECTORY_SEPARATOR);
define('VIEWS', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR);
define('SCRIPTS', dirname($_SERVER['SCRIPT_NAME']) . DIRECTORY_SEPARATOR);
define('PREFIX', "http://dbpedia.org/resource/");

$req_uri = $_SERVER['REQUEST_URI'];
$formatted_uri = preg_replace("/^\/index.php/","",$req_uri);
$formatted_uri = preg_split("/\?/",$formatted_uri)[0];


$router = new Router($formatted_uri);

error_log($formatted_uri);

$router->get('/', 'App\Controllers\IndexController@index');
$router->get('/accueil', 'App\Controllers\IndexController@index');
$router->get('', 'App\Controllers\IndexController@index');

$router->get('/ville', 'App\Controllers\VilleController@index');
$router->get('/ville/index', 'App\Controllers\VilleController@index');
$router->get('/ville/show/:resource', 'App\Controllers\VilleController@show');
$router->get('/ville/search', 'App\Controllers\VilleController@search');

$router->get('/religion', 'App\Controllers\ReligionController@index');
$router->get('/religion/index', 'App\Controllers\ReligionController@index');
$router->get('/religion/show/:resource', 'App\Controllers\ReligionController@show');
$router->get('/religion/search', 'App\Controllers\ReligionController@search');

$router->get('/batiment-religieux', 'App\Controllers\BatimentReligieuxController@index');
$router->get('/batiment-religieux/index', 'App\Controllers\BatimentReligieuxController@index');
$router->get('/batiment-religieux/search', 'App\Controllers\BatimentReligieuxController@search');
$router->get('/batiment-religieux/show/:resource', 'App\Controllers\BatimentReligieuxController@show');



try {
    $router->run();
} catch (NotFoundException $e) {
    return $e->error404();
}



