<?php


namespace App\Models;


use EasyRdf\Sparql\Client;
use EasyRdf\Sparql\Result;

class Ville
{
    public static function findAll()
    {
        $client = new Client("https://dbpedia.org/sparql", "");
        $query = "
            select distinct ?label ?location as ?ville where {
                ?temple rdf:type dbo:ReligiousBuilding.
                ?temple dbp:location ?location.
                ?location dbo:country dbr:Sri_Lanka.
                ?location rdfs:label ?label.
                FILTER (lang(?label) = 'en')
            } ORDER BY ?label
        ";


        return $client->query($query);
    }

    public static function findByResource(string $resource)
    {
        $client = new Client("https://dbpedia.org/sparql", "");
        $resource_URI = PREFIX . $resource;
        $query = "
            SELECT DISTINCT 
                ?label 
                ?ville 
                ?postalCode 
                ?utcOffset 
                ?settlementType 
                ?populationTotal 
                (SAMPLE(?abstract) as ?abstract)
            WHERE {
                BIND(<${resource_URI}> as ?ville).
                ?ville rdfs:label ?label.
               
                OPTIONAL {
                    ?ville dbo:abstract ?abstract
                    FILTER (lang(?abstract) = 'en')
                }
                OPTIONAL {
                    ?ville dbo:postalCode ?postalCode.
                }
                OPTIONAL {
                    ?ville dbo:utcOffset ?utcOffset.
                }
                OPTIONAL {
                    ?ville dbp:settlementType ?settlementType.
                    FILTER (lang(?settlementType) = 'en')
                }
                OPTIONAL {
                    ?ville dbp:populationTotal ?populationTotal.
                }
                FILTER (lang(?label) = 'en')
                
            } ORDER BY ?label
        ";

        return $client->query($query)[0];
    }

    public static function findContainingWord(string $word): Result
    {

        $client = new Client("https://dbpedia.org/sparql", "");
        $query = "
            SELECT DISTINCT 
                ?location as ?ville
                (MAX(?label) as ?label)
            WHERE {
            ?temple rdf:type dbo:ReligiousBuilding.
                ?temple dbp:location ?location.
                ?location dbo:country dbr:Sri_Lanka.
                ?location rdfs:label ?label.
              FILTER (langMatches(lang(?label),'en'))
              FILTER contains(lcase(str(?label)),lcase('${word}'))
            }
            GROUP BY ?location
            ";
        return $client->query($query);
    }
}