<?php


namespace App\Models;


use EasyRdf\Sparql\Client;
use EasyRdf\Sparql\Result;

class BatimentReligieux
{

    public static function findAll(): Result
    {
        $client = new Client("https://dbpedia.org/sparql", "");

        $query = "
             select distinct ?label ?temple where {
                ?temple rdf:type dbo:ReligiousBuilding.
                ?temple dbp:location ?location.
                ?location dbo:country dbr:Sri_Lanka.
                ?temple rdfs:label ?label.
                FILTER (lang(?label) = 'en')
            } ORDER BY ?label
        ";


        return $client->query($query);
    }

    public static function findContainingWord(string $word): Result
    {

        $client = new Client("https://dbpedia.org/sparql", "");
        $query = "
            SELECT DISTINCT ?temple
              (MAX(?label) as ?label)
            WHERE { 
              ?temple a dbo:ReligiousBuilding; 
                      rdfs:label ?label.
              FILTER (langMatches(lang(?label),'en'))
              FILTER contains(lcase(str(?label)),lcase('${word}'))
            }
            GROUP BY ?temple
            ";
        return $client->query($query);
    }

    public static function findByResource(string $resource)
    {
        $client = new Client("https://dbpedia.org/sparql", "");
        $resource_URI = PREFIX . $resource;
        $query = "
            select distinct ?label ?temple ?lat ?long ?abstract as ?abstract where {
                BIND(<${resource_URI}> as ?temple).
                ?temple rdfs:label ?label.
                OPTIONAL {
                    ?temple dbo:abstract ?abstract.
                }
                OPTIONAL {
                    ?temple geo:lat ?lat.
                }
                OPTIONAL {
                    ?temple geo:long ?long.
                }
                FILTER (lang(?label) = 'en')
            } ORDER BY ?label
        ";
        return $client->query($query)[0];
    }



}