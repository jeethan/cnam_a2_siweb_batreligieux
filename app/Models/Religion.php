<?php


namespace App\Models;


use EasyRdf\Sparql\Client;
use EasyRdf\Sparql\Result;

class Religion
{
    public static function findAll()
    {
        $client = new Client("https://dbpedia.org/sparql", "");
        $query = "
            select ?label SAMPLE(?religion) as ?religion where {
                ?temple rdf:type dbo:ReligiousBuilding.
                ?temple dbp:location ?location.
                ?location dbo:country dbr:Sri_Lanka.
                ?temple dbp:religiousAffiliation ?religion.
                ?religion rdfs:label ?label.
                FILTER langMatches(lang(?label),'en').
            }
        ";

        return $client->query($query);
    }

    public static function findByResource(string $resource)
    {
        $client = new Client("https://dbpedia.org/sparql", "");
        $resource_URI = PREFIX . $resource;
        $query = "
            select distinct ?label ?religion (SAMPLE(?abstract) as ?abstract) where {
                BIND(<${resource_URI}> as ?religion).
                ?religion rdfs:label ?label.               
                OPTIONAL {
                    ?religion dbo:abstract ?abstract
                    FILTER (lang(?abstract) = 'en')     
                }   
                FILTER (lang(?label) = 'en')
            } ORDER BY ?label
        ";
        return $client->query($query)[0];
    }

    public static function findContainingWord(string $word): Result
    {

        $client = new Client("https://dbpedia.org/sparql", "");
        $query = "
            SELECT DISTINCT ?religion
                (MAX(?label) as ?label)
            WHERE {
            ?temple rdf:type dbo:ReligiousBuilding.
                ?temple dbp:location ?location.
                ?location dbo:country dbr:Sri_Lanka.
                ?temple dbp:religiousAffiliation ?religion.
                ?religion rdfs:label ?label.
              FILTER (langMatches(lang(?label ),'en'))
              FILTER contains(lcase(str(?label)),lcase('${word}'))
            }
            GROUP BY ?religion
            ";
        return $client->query($query);
    }
}