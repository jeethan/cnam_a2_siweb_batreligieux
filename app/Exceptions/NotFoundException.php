<?php

namespace App\Exceptions;

use Exception;
use SmartyException;
use Throwable;
use Smarty;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

class NotFoundException extends Exception
{

    /**
     * @var Environment
     */
    private $twig;

    public function __construct($message = "", $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $loader = new FilesystemLoader(VIEWS);
        $this->twig = new Environment($loader, [
            'cache' => TWIG_CACHE,
            'debug' => true
        ]);
    }

    public function error404()
    {
        http_response_code(404);
        try {
            echo $this->twig->render('errors/404.twig');
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
        }
    }
}
