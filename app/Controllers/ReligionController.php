<?php


namespace App\Controllers;


use App\Models\Religion;



class ReligionController extends Controller
{
    public function index()
    {
        $result = Religion::findAll();
        echo $this->twig->render("religion/index.twig", [
            "result" => $result
        ]);
    }

    public function show(string $resource){
        $religion = Religion::findByResource($resource);
        echo $this->twig->render("religion/show.twig", [
            "religion" => $religion
        ]);
    }

    public function search()
    {
        if (!isset($_GET["query"])) {
            echo $this->twig->render("religion/results.twig", [
                "result" => [],
                "query" => null
            ]);
        }

        $query = $_GET["query"];
        $result = Religion::findContainingWord($query);
        echo $this->twig->render("religion/results.twig", [
            "result" => $result,
            "query" => $query
        ]);
    }

}