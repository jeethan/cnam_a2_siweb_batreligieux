<?php

namespace App\Controllers;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;



abstract class Controller {


    /**
     * @var Environment
     */
    protected $twig;

    public function index() {
        echo "TODO";
    }

    public function __construct()
    {
        $loader = new FilesystemLoader(VIEWS);
        $this->twig = new Environment($loader, [
            'cache' => TWIG_CACHE,
            'debug' => true
        ]);
        $this->twig->addExtension(new \Twig\Extension\DebugExtension());
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
    }
}
