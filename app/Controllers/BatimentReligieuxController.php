<?php


namespace App\Controllers;


use App\Models\BatimentReligieux;
use App\Models\Religion;
use App\Models\Ville;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class BatimentReligieuxController extends Controller
{

    public function index()
    {
        $result = BatimentReligieux::findAll();
        echo $this->twig->render("bats/index.twig", [
            "result" => $result
        ]);
    }

    public function search()
    {
        if (!isset($_GET["query"])) {
            echo $this->twig->render("bats/results.twig", [
                "result" => [],
                "query" => null
            ]);
        }

        $query = $_GET["query"];
        $result = BatimentReligieux::findContainingWord($query);
        echo $this->twig->render("bats/results.twig", [
            "result" => $result,
            "query" => $query
        ]);

    }

    public function show(string $resource){
        $temple = BatimentReligieux::findByResource($resource);
        echo $this->twig->render("bats/show.twig", [
            "temple" => $temple
        ]);
    }

}