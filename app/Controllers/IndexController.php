<?php


namespace App\Controllers;


use App\Models\BatimentReligieux;
use Exception;
use Smarty;
use SmartyException;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class IndexController extends Controller
{

    public function index()
    {

        try {
            echo $this->twig->render("index.twig");
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
            echo $e->getMessage();
        }

    }

    public function test(){

    }


}