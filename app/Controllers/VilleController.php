<?php


namespace App\Controllers;


use App\Models\BatimentReligieux;
use App\Models\Ville;

class VilleController extends Controller
{

    public function index()
    {
        $result = Ville::findAll();
        echo $this->twig->render("ville/index.twig", [
            "result" => $result
        ]);
    }

    public function show(string $resource){
        $ville = Ville::findByResource($resource);
        echo $this->twig->render("ville/show.twig", [
            "ville" => $ville
        ]);
    }

    public function search()
    {
        if (!isset($_GET["query"])) {
            echo $this->twig->render("ville/results.twig", [
                "result" => [],
                "query" => null
            ]);
        }

        $query = $_GET["query"];
        $result = Ville::findContainingWord($query);
        echo $this->twig->render("ville/results.twig", [
            "result" => $result,
            "query" => $query
        ]);
    }

}